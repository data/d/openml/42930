# OpenML dataset: alcohol-qcm-sensor

https://www.openml.org/d/42930

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: M. Fatih Adak, Peter Lieberzeit, Purim Jarujamrus, Nejat Yumusak

**Source**: [UCI](https://archive.ics.uci.edu/ml/datasets/Alcohol+QCM+Sensor+Dataset) - 2019   
**Please cite**: [M. Fatih Adak, Peter Lieberzeit, Purim Jarujamrus, Nejat Yumusak, Classification of alcohols obtained by QCM sensors with different characteristics using ABC based neural network, Engineering Science and Technology, an International Journal, 2019, ISSN 2215-0986](https://www.sciencedirect.com/science/article/pii/S2215098619303337?via%3Dihub)


In the dataset there are 5 types of dataset.QCM3, QCM6, QCM7, QCM10, QCM12In each of dataset, there is alcohol classification of five types,1-octanol, 1-propanol, 2-butanol, 2-propanol, 1-isobutanolIn this file the 5 datasets are merged into 1 dataset, with each25 entries representing 1 original dataset, respectively.The gas sample is passed through the sensor in five differentconcentrations. These concentrations are:Concentration	Air ratio (ml)	Gas ratio (ml)1       0.799	0.2012	0.700	0.3003	0.600	0.4004	0.501	0.4995	0.400	0.600There are two channels in the sensor. One of these circles forms channel 1,and the other forms channel 2. MIP and MP ratios used in the QCM sensors are,Sensor name	MIP ratio	NP ratioQCM3	1	1QCM6	1	0QCM7	1	0.5QCM10	1	2QCM12	0	1

In this file, the 5 datasets are merged into 1, with each 25 rows representing 1 original dataset.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42930) of an [OpenML dataset](https://www.openml.org/d/42930). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42930/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42930/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42930/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

